package uk.co.ataulmunim.android.example.grokkingnativeab;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


public class NativeDoneDiscard extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native_done_discard);
        
        setDoneDiscard();
    }

    /**
     * Sets the DISCARD | DONE custom action bar.
     * 
     * TargetApi is better than SuppressLint (as it's more granular) but this
     * is just to show that this approach works too.
     * 
     * It's used in this instance to tell Lint that we've taken care of the
     * potential missing API when we instantiate the LayoutInflater.
     */
    @SuppressLint("NewApi")
    public void setDoneDiscard() {
    	ActionBar actionBar = getActionBar();
    	
    	LayoutInflater inflater;
        
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            inflater = (LayoutInflater) getSystemService(
                    LAYOUT_INFLATER_SERVICE);
        } else {
            inflater = (LayoutInflater) getActionBar().getThemedContext()
                    .getSystemService(LAYOUT_INFLATER_SERVICE);    
        }
        
        final View doneDiscard = inflater.inflate(
                R.layout.actionbar_custom_view_done_discard, null);
        
        doneDiscard.findViewById(R.id.actionbar_done).setOnClickListener(
                new View.OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(NativeDoneDiscard.this, R.string.toast_done,
                                Toast.LENGTH_SHORT).show(); 
                        finish();
                    }
                });
        
        doneDiscard.findViewById(R.id.actionbar_discard).setOnClickListener(
                new View.OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(NativeDoneDiscard.this,
                                R.string.toast_discard, Toast.LENGTH_SHORT)
                                        .show();
                        finish();
                    }
                });
        
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE
        );
        
        final int matchParent = ViewGroup.LayoutParams.MATCH_PARENT;
        actionBar.setCustomView(doneDiscard, new ActionBar.LayoutParams(
                matchParent, matchParent));
    }
}
