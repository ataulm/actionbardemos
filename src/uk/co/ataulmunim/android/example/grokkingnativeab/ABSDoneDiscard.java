package uk.co.ataulmunim.android.example.grokkingnativeab;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;

import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

// Note, we extend from SherlockActivity here
public class ABSDoneDiscard extends SherlockActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	// if you set this after onCreate, it includes the activity title
    	// on pre honeycomb
    	setTheme(R.style.Theme_Sherlock_Light_DarkActionBar);
    	
    	super.onCreate(savedInstanceState);
    	
    	setContentView(R.layout.activity_abs_done_discard);
        
        setDoneDiscard();
    }
    
    /**
     * Sets the DISCARD | DONE custom action bar.
     * 
     * Here's an example using @TargetApi instead of @SuppressLint like in the
     * demo for the native action bar, {@link NativeDoneDiscard}.
     * 
     * It's used in this instance to tell Lint that we've taken care of the
     * potential missing API when we instantiate the LayoutInflater.
     * 
     * TODO: why isn't Lint complaining that getThemedContext is API 14, but
     * I've only TargetApi'd up to 11?
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void setDoneDiscard() {
    	// Note, we're using com.actionbarsherlock.app.ActionBar
    	// and a different method to retrieve the action bar
    	ActionBar actionBar = getSupportActionBar();
    	
    	LayoutInflater inflater;
    	
    	if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            inflater = (LayoutInflater) getSystemService(
                    LAYOUT_INFLATER_SERVICE);
        } else {
            inflater = (LayoutInflater) actionBar.getThemedContext()
                    .getSystemService(LAYOUT_INFLATER_SERVICE);    
        }
    	
    	final View doneDiscard = inflater.inflate(
                R.layout.actionbar_custom_view_done_discard, null);
        
        doneDiscard.findViewById(R.id.actionbar_done).setOnClickListener(
                new View.OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ABSDoneDiscard.this, R.string.toast_done,
                                Toast.LENGTH_SHORT).show(); 
                        finish();
                    }
                });
        
        doneDiscard.findViewById(R.id.actionbar_discard).setOnClickListener(
                new View.OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ABSDoneDiscard.this,
                                R.string.toast_discard, Toast.LENGTH_SHORT)
                                        .show();
                        finish();
                    }
                });
        
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE
        );
        
        final int matchParent = ViewGroup.LayoutParams.MATCH_PARENT;
        actionBar.setCustomView(doneDiscard, new ActionBar.LayoutParams(
                matchParent, matchParent));
    }
    

}
