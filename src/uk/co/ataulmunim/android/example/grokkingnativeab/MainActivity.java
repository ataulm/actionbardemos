package uk.co.ataulmunim.android.example.grokkingnativeab;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Hide the native one from devices below API 11
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
        	findViewById(R.id.button_native).setVisibility(View.GONE);
        }
    }
    
    public void openNativeDemo(View button) {
    	startActivity(new Intent(this, NativeDoneDiscard.class));
    }
    
    public void openABSDemo(View button) {
    	startActivity(new Intent(this, ABSDoneDiscard.class));
    }
}
